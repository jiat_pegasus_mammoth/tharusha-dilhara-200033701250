package com.jiat;

import com.jiat.db.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "UpdateProcess",urlPatterns = "/updateProcess")
public class UpdateProcess extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String uid = req.getParameter("uid");
        String name = req.getParameter("name");
        String email = req.getParameter("email");
        String mobile = req.getParameter("mobile");
        String password = req.getParameter("password");

        Connection connection = null;

        try {

            connection = DBConnection.getConnection();

            DBConnection.iud("UPDATE `user` SET `name`='"+name+"',`email`='"+email+"',`mobile`='"+mobile+"',`password`='"+password+"' WHERE `id`='"+uid+"'");



        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (connection != null) {

                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }

            resp.sendRedirect(req.getContextPath() + "/index.jsp");

        }
    }
}
