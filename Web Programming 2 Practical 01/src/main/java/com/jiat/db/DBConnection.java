package com.jiat.db;

import com.jiat.util.ApplicationProperties;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class DBConnection {
    private static Connection connection;

    public static Connection getConnection() throws Exception{
        ApplicationProperties applicationProperties = ApplicationProperties.getInstance();

        Class.forName(applicationProperties.get("sql.connection.driver"));
        connection = DriverManager.getConnection(applicationProperties.get("sql.connection.url"),applicationProperties.get("sql.connection.username"),applicationProperties.get("sql.connection.password"));
        return connection;
    }

    public static void iud(String query) {
        try {
            connection.createStatement().execute(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ResultSet search(String query) throws Exception {
        return connection.createStatement().executeQuery(query);
    }
}
