package com.jiat;

import com.jiat.db.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "Login", urlPatterns = "/login")
public class Login extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        String name = req.getParameter("name");
        String email = req.getParameter("email");
        String mobile = req.getParameter("mobile");
        String password = req.getParameter("password");

        Connection connection = null;

        try {

            connection = DBConnection.getConnection();

            DBConnection.iud("INSERT INTO `user`(`name`,`email`,`mobile`,`password`) VALUES ('"+name+"','"+email+"','"+mobile+"','"+password+"')");

            ResultSet result = DBConnection.search("SELECT * FROM `user`");

            resp.getWriter().write("<h1><u>Users</u></h1>");
            resp.getWriter().write("<br>");
            resp.getWriter().write("<br>");

            while (result.next()) {
                resp.getWriter().write(result.getString("id"));
                resp.getWriter().write("<br>");
                resp.getWriter().write("UID : " + result.getString("id"));
                resp.getWriter().write("<br>");
                resp.getWriter().write("Name : " + result.getString("name"));
                resp.getWriter().write("<br>");
                resp.getWriter().write("Email : " + result.getString("email"));
                resp.getWriter().write("<br>");
                resp.getWriter().write("Password : " + result.getString("password"));
                resp.getWriter().write("<br>");
                resp.getWriter().write("<a href='update?uid="+result.getString("id")+"'>Update</a>");
                resp.getWriter().write("<br>");
                resp.getWriter().write("<br>");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (connection != null) {

                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }

        }
    }
}
