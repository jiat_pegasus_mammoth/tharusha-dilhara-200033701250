package com.jiat;

import com.jiat.db.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "Update",urlPatterns = "/update")
public class Update extends HttpServlet {
    private Connection connection;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        String uid = req.getParameter("uid");

        Connection connection = null;

        try {

            connection = DBConnection.getConnection();

            ResultSet result = DBConnection.search("SELECT * FROM `user` WHERE id='"+uid+"'");
            result.next();
            String name = result.getString("name");
            String email = result.getString("email");
            String mobile = result.getString("mobile");
            String password = result.getString("password");

            resp.getWriter().write("<form action='updateProcess' method='POST'>");
            resp.getWriter().write("<span>ID</span>");
            resp.getWriter().write("<input type='text' name='uid' value="+uid+" disabled>");
            resp.getWriter().write("<br><br>");
            resp.getWriter().write("<span>Name</span>");
            resp.getWriter().write("<input type='text' name='name' value="+name+">");
            resp.getWriter().write("<br><br>");
            resp.getWriter().write("<span>Email</span>");
            resp.getWriter().write("<input type='text' name='email' value="+email+">");
            resp.getWriter().write("<br><br>");
            resp.getWriter().write("<span>Mobile</span>");
            resp.getWriter().write("<input type='text' name='mobile' value="+mobile+">");
            resp.getWriter().write("<br><br>");
            resp.getWriter().write("<span>Password</span>");
            resp.getWriter().write("<input type='text' name='password' value="+password+">");
            resp.getWriter().write("<br><br>");
            resp.getWriter().write("<button type='submit' value='Update'>Update</button>");
            resp.getWriter().write("<br><br>");

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (connection != null) {

                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }

        }
    }
}
